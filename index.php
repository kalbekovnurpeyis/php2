<?php

$url = $_SERVER['REQUEST_URI'];

require __DIR__ . '/autoload.php';

$controller = new \App\Controllers\News();

$action = !empty($_GET['action']) ? $_GET['action'] : 'Index';

try {
    $controller->action($action);
} catch (\App\Exceptions\Core $e) {
    echo 'Возникло исключение ' . $e->getMessage();
} catch (PDOException $e) {
    echo 'Что то не так с базой';
}