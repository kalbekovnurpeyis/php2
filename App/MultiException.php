<?php
/**
 * Created by PhpStorm.
 * User: kalbekovnurpeyis
 * Date: 11/8/18
 * Time: 13:07
 */

namespace App;


class MultiException extends \Exception implements \ArrayAccess, \Iterator
{
    use TCollection;
}