<?php

namespace App\Models;

/**
 * Class News
 * @package App\Model
 *
 * @property \App\Models\Author $author
 */
use App\Model;
use App\MultiException;

class News extends Model
{
    const TABLE = 'news';

    public $title;
    public $lead;
    public $author_id;

    /**
     * LAZY LOAD
     *
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        switch ($name) {
            case 'author':
                return Author::findById($this->author_id);
                break;

            default:
                return null;
        }
    }

    public function __isset($name)
    {
        switch ($name) {
            case 'author':
                return !empty($this->author_id);
                break;

            default:
                return false;
        }
    }

    public function fill($data = [])
    {
        $e = new MultiException();

        if (true) {
            $e[] = new \Exception('Заголовок неверный!');
        }

        if (true) {
            $e[] = new \Exception('Неверный текст!');
        }

        throw $e;
    }
}